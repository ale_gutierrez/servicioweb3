package android.curso.serviciosweb4;

import com.google.gson.annotations.SerializedName;

/**
 * Created by agutierrez on 23/04/2016.
 */
public class Dispositivo {

    @SerializedName("mac")
    public String mac;

    public String getMac(){return mac;}

    public void setMac(String mac){this.mac=mac;}
}
