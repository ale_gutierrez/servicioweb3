package android.curso.serviciosweb4;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Resultado {


    @SerializedName("correcto")
    public int correcto;

    @SerializedName("usuario")
    public List<Usuario> usuario;

    @SerializedName("manzano")
    public List<Manzano> manzano;

    public int getCorrecto() {
        return correcto;
    }

    public void setCorrecto(int correcto) {
        this.correcto = correcto;
    }

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }

    public List<Manzano> getManzano() {
        return manzano;
    }

    public void setManzano(List<Manzano> manzano) {
        this.manzano = manzano;
    }

}

