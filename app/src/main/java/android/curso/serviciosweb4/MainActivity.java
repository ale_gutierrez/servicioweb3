package android.curso.serviciosweb4;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Button get;
    private Button post;
    private TableLayout tabla;
    private TableRow fila;
    TableRow.LayoutParams layoutFila;

    private String nuevo_mac;

    private Context context;

    private List<Usuario> lstUsuarios=new ArrayList<Usuario>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context=this;

        get = (Button) findViewById(R.id.get);
        post = (Button) findViewById(R.id.post);
        tabla = (TableLayout) findViewById(R.id.tabla);

        layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);

    /*    get.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                tabla.removeAllViews();
                MetodoGet();
            }
        });*/

        post.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Dialog popup = new Dialog(MainActivity.this);
                popup.setContentView(R.layout.popup);
                popup.setTitle("Verficador de MAC");

                final TextView Nmac = (TextView) popup.findViewById(R.id.nuevo_mac);

                Button enviar = (Button) popup.findViewById(R.id.enviar);

                popup.show();

                enviar.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        popup.dismiss();
                        nuevo_mac = Nmac.getText().toString();
                        MetodoPost();
                    }
                });
            }
        });
    }

    // agragar filas de lo que se recibe del JSON
/*    private void agregarFilas(String nombre, String departamento, String sueldo) {
        fila = new TableRow(this);
        fila.setLayoutParams(layoutFila);

        TextView nombre_empleado = new TextView(this);
        TextView departamento_empleado = new TextView(this);
        TextView sueldo_empleado = new TextView(this);

        nombre_empleado.setText(nombre);
        departamento_empleado.setText(departamento);
        sueldo_empleado.setText(sueldo);

        nombre_empleado.setBackgroundResource(R.drawable.celda_cuerpo);
        departamento_empleado.setBackgroundResource(R.drawable.celda_cuerpo);
        sueldo_empleado.setBackgroundResource(R.drawable.celda_cuerpo);


        sueldo_empleado.setGravity(Gravity.RIGHT);

        //Agregamos las vistas a la fila
        fila.addView(nombre_empleado);
        fila.addView(departamento_empleado);
        fila.addView(sueldo_empleado);
        tabla.addView(fila);

    }*/
    //----------------------------------------------------------------------------------------------
    // hago uso de los servicios web que necesito
    private ProgressDialog pDialog;
    //private static String link_get = "http://10.1.6.38/monitoreo/cswUsuario/obtenerLogin";
    private static String link_post = "http://10.1.6.38/monitoreo/cswUsuario2/obtenerLogin";

    /*private void MetodoGet()
    {
        pDialog = new ProgressDialog(context);
        HashMap<String, String> parametros = new HashMap();
        pDialog.setMessage("Cargando...");
        pDialog.show();

        JsonObjectRequest jsArrayRequest = new JsonObjectRequest(
                Request.Method.GET,
                link_get,
                new JSONObject(parametros),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (pDialog != null)
                            if (pDialog.isShowing())
                                pDialog.hide();
                        try
                        {
                            Resultado resultadoJson = new Gson().fromJson(response.toString(), Resultado.class);
                            if (resultadoJson.getCorrecto()==1)
                            {
                                agregarFilas("NOMBRE", "DEPARTAMENTO", "SUELDO");

                                for (Empleado empleado : resultadoJson.getEmpleados()) {
                                    agregarFilas(
                                            empleado.getNombres(),
                                            empleado.getDepartamento(),
                                            String.valueOf(empleado.getSueldo())
                                    );
                                }
                                Toast.makeText(getApplicationContext(),
                                        "Lista de empleados actualizada", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Error en la conexión", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception ex) {
                            Log.e("Ejemplo", "Respuesta Volley:" + ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (pDialog != null)
                            if (pDialog.isShowing())
                                pDialog.hide();
                        // Manejo de errores
                        Log.e("Ejemplo", "Error:" + error.getMessage());

                    }
                });
        WsPeticiones.getInstance(context).addToRequestQueue(jsArrayRequest);
    }*/

    private void MetodoPost()
    {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Cargando...");
        pDialog.show();

        StringRequest jsArrayRequest = new StringRequest(
                Request.Method.POST,
                link_post,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (pDialog != null)
                            if (pDialog.isShowing())
                                pDialog.hide();
                        try {
                            Log.e("Respuesta", response);
                            Resultado resultadoJson = new Gson().fromJson(response, Resultado.class);
                            if (resultadoJson.getCorrecto() == 1)
                            {
                                lstUsuarios=resultadoJson.getUsuario();
                                Log.e("Respuesta",new Gson().toJson(resultadoJson.getUsuario()));
                                Log.e("Respuesta",new Gson().toJson(resultadoJson.getManzano()));
                                // si ya esta registrado entonces se recibe datos de el servicio
                                Toast.makeText(getApplicationContext(),
                                        "Esta mac ya esta registrada, "+resultadoJson.getUsuario().get(0).getNombres(), Toast.LENGTH_SHORT).show();

                                new Insertar().execute();
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Error en la conexión", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            Log.e("Ejemplo", "Respuesta Volley:" + ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (pDialog != null)
                            if (pDialog.isShowing())
                                pDialog.hide();
                        // Manejo de errores
                        Log.e("Ejemplo", "Error:" + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                "Error en la conexión", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> pars = new HashMap<String, String>();
                pars.put("Content-Type", "application/x-www-form-urlencoded");
                return pars;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> parametros = new HashMap();


                Dispositivo dispositivo=new Dispositivo();
                dispositivo.setMac(nuevo_mac);

                List<Dispositivo> lstDispositivo=new ArrayList<Dispositivo>();
                lstDispositivo.add(dispositivo);
                Peticion peticion=new Peticion();
                peticion.setDispositivo(lstDispositivo);

                Log.e("Enviando",new Gson().toJson(peticion));
                parametros.put("tablet", new Gson().toJson(peticion));
                //parametros.put("departamento", nuevo_depto);
                //parametros.put("sueldo", nuevo_sueldo);
                return parametros;
            }
        };
        WsPeticiones.getInstance(context).addToRequestQueue(jsArrayRequest);
    }

    class Peticion{
        @SerializedName("dispositivo")
        public List<Dispositivo> dispositivo;

        public List<Dispositivo> getDispositivo() {
            return dispositivo;
        }

        public void setDispositivo(List<Dispositivo> dispositivo) {
            this.dispositivo = dispositivo;
        }
    }

    class Insertar extends AsyncTask<String,String, String>{

        @Override
        protected String doInBackground(String... params) {
            Base_datos base_datos=new Base_datos(context,1);
            SQLiteDatabase db=base_datos.getWritableDatabase();

            for(Usuario usuario : lstUsuarios)
            {
                try {
                    ContentValues values = new ContentValues();
                    values.put("nombres", usuario.getNombres());
                    values.put("apellidos", usuario.getApellidos());

                    long idGenerado = db.insert("usuarios", null, values);
                    if (idGenerado > 0)
                        Log.e("Insertado", "" + idGenerado);
                    else
                        Log.e("Insertado", "" + idGenerado);
                }catch (Exception ex)
                {
                    Log.e("Insertado", "" + ex.getMessage());
                }
            }

            return null;
        }

        protected void onPostExecute(String resultado){
            super.onPostExecute(resultado);
        }
    }
}
