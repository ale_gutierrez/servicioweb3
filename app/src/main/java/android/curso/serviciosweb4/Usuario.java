package android.curso.serviciosweb4;

import com.google.gson.annotations.SerializedName;

/**
 * Created by agutierrez on 23/04/2016.
 */
public class Usuario {
    @SerializedName("nombres")
    public String nombres;
    @SerializedName("apellidos")
    public String apellidos;
    @SerializedName("documento")
    public String documento;
    @SerializedName("telefono")
    public String telefono;
    @SerializedName("correo")
    public String correo;
    @SerializedName("iddep")
    public String iddep;
    @SerializedName("idcargo")
    public String idcargo;
    @SerializedName("usuarioreg")
    public String usuarioreg;
    @SerializedName("fechareg")
    public String fechareg;
    @SerializedName("usuariomod")
    public String usuariomod;
    @SerializedName("fechamod")
    public String fechamod;
    @SerializedName("usuario")
    public String usuario;
    @SerializedName("password")
    public String password;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getIddep() {
        return iddep;
    }

    public void setIddep(String iddep) {
        this.iddep = iddep;
    }

    public String getIdcargo() {
        return idcargo;
    }

    public void setIdcargo(String idcargo) {
        this.idcargo = idcargo;
    }

    public String getUsuarioreg() {
        return usuarioreg;
    }

    public void setUsuarioreg(String usuarioreg) {
        this.usuarioreg = usuarioreg;
    }

    public String getFechareg() {
        return fechareg;
    }

    public void setFechareg(String fechareg) {
        this.fechareg = fechareg;
    }

    public String getUsuariomod() {
        return usuariomod;
    }

    public void setUsuariomod(String usuariomod) {
        this.usuariomod = usuariomod;
    }

    public String getFechamod() {
        return fechamod;
    }

    public void setFechamod(String fechamod) {
        this.fechamod = fechamod;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
