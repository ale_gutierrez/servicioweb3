package android.curso.serviciosweb4;

import com.google.gson.annotations.SerializedName;

/**
 * Created by agutierrez on 23/04/2016.
 */
public class Manzano {

    @SerializedName("id")
    public int id;

    @SerializedName("idmanzana")
    public String idmanzana;

    @SerializedName("iddep")
    public String iddep;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdmanzana() {
        return idmanzana;
    }

    public void setIdmanzana(String idmanzana) {
        this.idmanzana = idmanzana;
    }

    public String getIddep() {
        return iddep;
    }

    public void setIddep(String iddep) {
        this.iddep = iddep;
    }
}
